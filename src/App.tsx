import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { AppProvider } from "./redux/AppContext";

import PageLayout from "./pages/Layout";

import AboutPage from "./pages/About";
import HomePage from "./pages/Home";
import HowItWorksPage from "./pages/HowItWorks";
import BoxesPage from "./pages/Boxes";
import BoxPage from "./pages/Box";
import CreateBoxPage from "./pages/CreateBox";

// todo fix default route to home maybe???

function App() {

	return (
		<AppProvider>
			<BrowserRouter>
				<PageLayout>
					<Routes>
						<Route path="/" element={<HomePage />} />
						<Route path="/about" element={<AboutPage />} />
						<Route path="/how-it-works" element={<HowItWorksPage />} />
						<Route path="/boxes" element={<BoxesPage />} />
						<Route path="/box" element={<BoxPage />} />
						<Route path="/create-box" element={<CreateBoxPage />} />
					</Routes>
				</PageLayout>
			</BrowserRouter>
		</AppProvider>
	);

}

export default App;
