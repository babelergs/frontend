import { ADDRESS_NETWORK_TYPE_CURRENT } from "../scripts/blockchainParameters";

import { validate_token_id } from "../scripts/walletConnector";

import { babelFeesContractErgoTreePrefix, babelFeesContractErgoTreeSuffix } from "./babelFeesContract";

let ergolib = import("ergo-lib-wasm-browser");

export function generate_babel_fee_ergo_tree_for_token(tokenID: string): string {

	if (!validate_token_id(tokenID)) {
		throw new Error("Invalid token ID provided to generate_babel_fee_ergo_tree_for_token function!");
	}

	return babelFeesContractErgoTreePrefix + tokenID + babelFeesContractErgoTreeSuffix;
}

export async function get_address_from_ergo_tree(ergoTreeString: string): Promise<string> {

	const wasm = (await ergolib);

	try {

		const contractErgoTree = wasm.ErgoTree.from_base16_bytes(ergoTreeString);

		const contractAddress = wasm.Address.recreate_from_ergo_tree(contractErgoTree);

		return contractAddress.to_base58(ADDRESS_NETWORK_TYPE_CURRENT);

	} catch (e: any) {
		throw new Error("Generation of address from ergoTree failed!");
	}

}

export async function generate_babel_fee_address_for_token(tokenID: string): Promise<string> {

	try {

		const contractErgoTreeString = generate_babel_fee_ergo_tree_for_token(tokenID);

		return get_address_from_ergo_tree(contractErgoTreeString);

	} catch (e: any) {
		throw new Error("Generation of babel fee address failed!");
	}

}

//expect "renderedValue" (from explorer API) for sigmaProp
export async function babel_fee_sigma_prop_to_address(sigmaProp: string): Promise<string> {

	const wasm = (await ergolib);

	try {

		const address = wasm.Address.p2pk_from_pk_bytes(
			Uint8Array.from(
				Buffer.from(sigmaProp, "hex")
			)
		).to_base58(ADDRESS_NETWORK_TYPE_CURRENT);

		return address;
		
	} catch (e: any) {
		return "Unknown format";
	}

}
