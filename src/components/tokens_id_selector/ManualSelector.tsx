import React, { useRef } from "react";

import "../../css/TokenIDSelector.css";

function ManualSelector(
	props: {
		setToken: (tokenID: string) => void
	}
) {

	const tokenIDInputElement = useRef<HTMLInputElement>(null);

	function selectTokenHandler() {
		if (tokenIDInputElement.current) {
			props.setToken(tokenIDInputElement.current.value);
		}
	}

	return (
		<div className="token-select">
			<input type="text" name="token_id" placeholder="Token ID" ref={tokenIDInputElement} />
			<button onClick={selectTokenHandler}>Select token</button>
		</div>
	);

}

export default ManualSelector;
