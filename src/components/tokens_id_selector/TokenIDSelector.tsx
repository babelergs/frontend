import React, { useState } from "react";

import { useNavigate } from "react-router-dom";

import { AppContext } from "../../redux/AppContext";
import { IState } from "../../redux/reducer";

import { wellKnownTokensList } from "../../scripts/wellKnownTokens";

import { validate_token_id } from "../../scripts/walletConnector";

import TokensSelector from "./TokensSelector";
import ManualSelector from "./ManualSelector";

import "../../css/TokenIDSelector.css";

enum RadioOptions {
	OptionNone,
	OptionWellKnown,
	OptionUsers,
	OptionManual
};

function renderSelector(
	selectedOption: number,
	state: IState,
	setToken: (tokenID: string) => void
): JSX.Element {

	switch (selectedOption) {

		case RadioOptions.OptionNone:
			return (<div>Please choose one from the options above</div>);
		//break;

		case RadioOptions.OptionWellKnown:
			return (
				<TokensSelector
					tokens={wellKnownTokensList}
					showIcons={true}
					noneText="There are no well known tokens specified"
					setToken={setToken}
				/>
			);
		//break;

		case RadioOptions.OptionUsers:
			return (
				<TokensSelector
					tokens={state.wallet.balance.tokens}
					showIcons={false}
					noneText="You don't own any tokens"
					setToken={setToken}
				/>
			);
		//break;

		case RadioOptions.OptionManual:
			return (
				<ManualSelector
					setToken={setToken}
				/>
			);
		//break;

		default:
			return (<div></div>);
		//break;
	}
}

function TokenIDSelector() {

	const { state } = React.useContext(AppContext);

	const [selectedOption, setSelectedOption] = useState<number>(RadioOptions.OptionNone);

	const navigate = useNavigate();

	function setToken(tokenID: string): void {
		if (!validate_token_id(tokenID)) {
			alert("Invalid token ID selected!");
			return;
		}
		navigate("/boxes?token_id=" + tokenID);
	}

	function radioChanged(e: any) {

		let tmp_value = RadioOptions.OptionNone;

		switch (e.target.value) {
			case "well_known":
				tmp_value = RadioOptions.OptionWellKnown;
				break;
			case "users_tokens":
				tmp_value = RadioOptions.OptionUsers;
				break;
			case "manual_id":
				tmp_value = RadioOptions.OptionManual;
				break;
			default: break;
		}

		setSelectedOption(tmp_value);
	}

	return (

		<div className="token-id-selector-main-div">

			<div className="token-id-selector-radio-div">
				<div>
					<input
						type="radio" name="input_type" value="well_known" id="radio1"
						onChange={radioChanged}
					/>
					<label htmlFor="radio1">Choose from well know tokens</label>
				</div>
				{
					(!state.wallet.connected) ?
						[] :
						<div>
							<input
								type="radio" name="input_type" value="users_tokens" id="radio2"
								onChange={radioChanged}
							/>
							<label htmlFor="radio2">Choose from tokens owned by you</label>
						</div>
				}
				<div>
					<input
						type="radio" name="input_type" value="manual_id" id="radio3"
						onChange={radioChanged}
					/>
					<label htmlFor="radio3">Manualy enter token ID</label>
				</div>
			</div>

			<div>{renderSelector(selectedOption, state, setToken)}</div>


		</div>
	);
}

export default TokenIDSelector;
