import React, { useRef } from "react";

import { IToken } from "../../scripts/walletConnector";
import { IWellKnownToken } from "../../scripts/wellKnownTokens";

import "../../css/TokenIDSelector.css";

function TokensSelector(
	props: {
		tokens: IWellKnownToken[] | IToken[],
		showIcons: boolean,
		noneText: string,
		setToken: (tokenID: string) => void
	}
) {

	const tokenIDSelectElement = useRef<HTMLSelectElement>(null);

	function selectChangeHandler() {

	}

	function selectTokenHandler() {
		if (tokenIDSelectElement.current) {
			props.setToken(props.tokens[parseInt(tokenIDSelectElement.current.value)].id)
		}
	}


	return (
		<div>
			{
				(props.tokens.length === 0) ?
					props.noneText :
					<div className="token-select">
						<select name="token-select" onChange={selectChangeHandler} ref={tokenIDSelectElement}>
							{
								props.tokens.map(
									(token: any, index: number) =>
										<option
											value={index}
											key={token.id}>
											{token.name + " - " + token.id}
										</option>
								)
							}
						</select>
						<button onClick={selectTokenHandler}>Select token</button>
					</div>
			}
		</div>
	);

}

export default TokensSelector;
