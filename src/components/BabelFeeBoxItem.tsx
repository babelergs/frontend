import React from "react";

import { Link } from "react-router-dom";

import { NANO_ERGS_IN_ONE_ERG } from "../scripts/blockchainParameters";

import { IBabelBox } from "../scripts/babelFeeBoxTools";
import { ITokenParams } from "../pages/Boxes";

import "../css/BabeFeeBoxesList.css"

function BabelFeeBoxItem(
	props: {
		babelFeeBox: IBabelBox,
		tokenParams: ITokenParams,
		showLink: boolean
	}
) {
	return (
		<div>
			<table className="babel-fee-box-table">
				<tbody>
					<tr>
						<th>Babel box ID:</th>
						<td>
							{
								props.showLink ?
									<Link to={"/box?box_id=" + props.babelFeeBox.boxId}>{props.babelFeeBox.boxId}</Link>
									: props.babelFeeBox.boxId

							}
						</td>
					</tr>
					<tr>
						<th>Price:</th>
						<td>{props.babelFeeBox.price} nanoERGs per token (it's smallest denomination)</td>
					</tr>
					<tr>
						<th>Intuitive price:</th>
						<td>
							By paying {1 * (10 ** (-props.tokenParams.info.decimals))} {
								props.tokenParams.info.name} you can get up to {
								props.babelFeeBox.price / NANO_ERGS_IN_ONE_ERG} ERG
						</td>
					</tr>
					<tr>
						<th>Available liquidity:</th>
						<td>{props.babelFeeBox.nanoERGs / NANO_ERGS_IN_ONE_ERG} ERG</td>
					</tr>
					<tr>
						<th>Owner address:</th>
						<td>{props.babelFeeBox.owner}</td>
					</tr>
				</tbody>
			</table>
		</div>
	);
}

export default BabelFeeBoxItem;
