import React from "react";

import { ITokenInfo } from "../scripts/walletConnector";

//todo show icon if available for that specified token (token is part of wellKnownTokensList)

import "../css/TokenInfoDisplay.css";

function TokenInfoDisplay(
	props: {
		tokenID: string,
		tokenInfo: ITokenInfo
	}
) {

	return (
		<div>
			<table className="token-info-display-table">
				<tbody>
					<tr>
						<th>ID:</th>
						<td>{props.tokenID}</td>
					</tr>
					<tr>
						<th>Name:</th>
						<td>{props.tokenInfo.name}</td>
					</tr>
					<tr>
						<th>Decimals:</th>
						<td>{props.tokenInfo.decimals}</td>
					</tr>
					<tr>
						<th>Description:</th>
						<td>{props.tokenInfo.description}</td>
					</tr>
				</tbody>
			</table>
		</div>
	);

}

export default TokenInfoDisplay;
