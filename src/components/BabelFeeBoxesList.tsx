import React, { useState, useEffect } from "react";

import { validate_token_id } from "../scripts/walletConnector";

import { IBabelBox, construct_babel_fee_boxes_list } from "../scripts/babelFeeBoxTools";

import { ITokenParams } from "../pages/Boxes";

import BabelFeeBoxItem from "./BabelFeeBoxItem";

interface IBabelBoxesListState {
	loaded: boolean,
	items: IBabelBox[]
}

const BABEL_BOXES_LIST_DEFAULT_STATE: IBabelBoxesListState = {
	loaded: false, items: []
};


function BabelFeeBoxesList(
	props: {
		tokenParams: ITokenParams,
		userAddress: string,
		onlyUsersBoxes: boolean
	}
) {

	const [babelBoxesListState, setBabelBoxesListState] = useState<IBabelBoxesListState>(BABEL_BOXES_LIST_DEFAULT_STATE);

	useEffect(() => {
		if (validate_token_id(props.tokenParams.id)) {
			construct_babel_fee_boxes_list(props.tokenParams.id).then(
				(result: IBabelBox[]) => {
					setBabelBoxesListState({
						loaded: true,
						items: (result.filter((box: IBabelBox) => ((!props.onlyUsersBoxes) || box.owner === props.userAddress)))
					});
				}
			).catch(() => { setBabelBoxesListState(BABEL_BOXES_LIST_DEFAULT_STATE) });
		}
	}, [props.tokenParams.id, props.onlyUsersBoxes, props.userAddress]);

	if (!validate_token_id(props.tokenParams.id)) { return (<div>Invalid token ID!</div>); }

	return (
		<div>
			{
				(!babelBoxesListState.loaded) ?
					<h2>Loading babel fee boxes...</h2> :
					<div>
						{
							(babelBoxesListState.items.length === 0) ?

								(
									props.onlyUsersBoxes ?
										<p>There don't exist any babel boxel belonging to your address</p> :
										<p>Currently there don't exist any babel fee boxes for this token</p>
								) :

								babelBoxesListState.items.map((box: IBabelBox) =>
									<BabelFeeBoxItem
										tokenParams={props.tokenParams}
										babelFeeBox={box} key={box.boxId}
										showLink={box.owner === props.userAddress}
									/>
								)
						}
					</div>
			}
		</div>
	);
}

export default BabelFeeBoxesList;
