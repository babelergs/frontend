import React, { useRef } from "react";

import { AppContext } from "../../redux/AppContext";

import {
	get_box_to_spend, IUTXOToken
} from "../../scripts/walletConnector";

import {
	validate_address, create_withdrawal_transaction, sign_tx, submit_tx, INautilusUTXO
} from "../../scripts/transactionsBuilder";

import "../../css/CreateBabelBoxForm.css";

async function construct_and_sign_withdrawal_transaction(boxId: string, address: string): Promise<string | null> {

	const bx: INautilusUTXO = await get_box_to_spend(boxId);
	const tkns: IUTXOToken[] = bx.assets;

	const unsigned_tx = await create_withdrawal_transaction(
		address,
		bx,
		parseInt(bx.value),
		tkns
	);

	const signed_tx = await sign_tx(unsigned_tx);
	if (!signed_tx) return null;

	const sent_tx = await submit_tx(signed_tx);
	if (!sent_tx) return null;

	return sent_tx;
}

function FullWithdrawalForm(
	props: {
		boxId: string,
		setTxSent: React.Dispatch<React.SetStateAction<string>>
	}
) {

	const { state } = React.useContext(AppContext);

	const withdrawalAddressInputElement = useRef<HTMLInputElement>(null);

	function addressChangedHandler() {

	}

	function useCurrentAddressHandler(e: React.MouseEvent<HTMLElement>) {
		e.preventDefault();
		if (withdrawalAddressInputElement.current) {
			withdrawalAddressInputElement.current.value = state.wallet.address;
			addressChangedHandler();
		}
	}

	function withdrawAssetsHandler(e: React.MouseEvent<HTMLElement>) {
		e.preventDefault();
		if (withdrawalAddressInputElement.current) {
			validate_address(withdrawalAddressInputElement.current.value).then((result: boolean) => {
				if (result && props.boxId !== null) {
					construct_and_sign_withdrawal_transaction(
						props.boxId,
						withdrawalAddressInputElement.current!.value).then(
							(res) => {
								if (res == null) alert("Transaction sending failed!");
								else {
									props.setTxSent(res);
								}
							});
				} else {
					alert("Invalid withdrawal address!");
				}
			});
		}
	}

	return (
		<div className="create-babel-box-main-div">
			<form action="">

				<div>
					<h3>Address:</h3>
					<input type="text" placeholder="Withdrawal address"
						onChange={addressChangedHandler} ref={withdrawalAddressInputElement}
					/>
					<button onClick={useCurrentAddressHandler}>
						Use current address
					</button>
				</div>

				<button onClick={withdrawAssetsHandler}>
					Withdraw assets
				</button>

			</form>
		</div>
	);

}

export default FullWithdrawalForm;
