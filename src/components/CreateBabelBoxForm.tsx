import React, { useState, useRef } from "react";

import { AppContext } from "../redux/AppContext";

import { MIN_NANO_ERGS_IN_BOX, NANO_ERGS_IN_ONE_ERG, TRANSACTION_CREATE_BABEL_BOX_FEE } from "../scripts/blockchainParameters";
import { BABELERGS_SERVICE_FEE_AMOUNT } from "../scripts/babelERGsSettings";

import { validate_address, create_transaction_babel_fee_box_creation, sign_tx, submit_tx } from "../scripts/transactionsBuilder";

import { ITokenParams } from "../pages/Boxes";

import "../css/CreateBabelBoxForm.css";

export interface IFees {
	blockchain: number,
	service: number
};

const FEES_DEFAULT_STATE: IFees = {
	blockchain: TRANSACTION_CREATE_BABEL_BOX_FEE,
	service: BABELERGS_SERVICE_FEE_AMOUNT
};

interface ICreateBoxSettings {
	ergs: number,
	price: number,
	owner: string
}

const CREATE_BOX_SETTINGS_DEFAULT_VALUE: ICreateBoxSettings = {
	ergs: MIN_NANO_ERGS_IN_BOX,
	price: 1,
	owner: ""
};

const fees: IFees = FEES_DEFAULT_STATE;

async function validate_form(settings: ICreateBoxSettings, walletBalance: number): Promise<boolean> {

	if (settings.ergs <= MIN_NANO_ERGS_IN_BOX) {
		alert("There has to be more than minimum amount of ERG in the box!");
		return false;
	}

	if ((settings.ergs + fees.blockchain + fees.service) > walletBalance) {
		alert("Wallet don't have enough ERGs!");
		return false;
	}


	const owner_valid = await validate_address(settings.owner);

	if (!owner_valid) {
		alert("Owner address is invalid!");
		return false;
	}

	if (settings.price > (settings.ergs - MIN_NANO_ERGS_IN_BOX)) {
		alert("Price is invalid - there wouldn't be enough ERGs to buy a single token in a babel swap!");
		return false;
	}

	return true;
}

function CreateBabelBoxForm(
	props: {
		tokenParams: ITokenParams,
		txSent: (txId: string) => void
	}
) {

	const { state } = React.useContext(AppContext);

	const [settings, setSettings] = useState<ICreateBoxSettings>(CREATE_BOX_SETTINGS_DEFAULT_VALUE);

	const ownerAddressInputElement = useRef<HTMLInputElement>(null);
	const ergsAmountInputElement = useRef<HTMLInputElement>(null);
	const priceInputElement = useRef<HTMLInputElement>(null);

	function addressChangedHandler() {
		setSettings((previous: ICreateBoxSettings) => {
			if (ownerAddressInputElement.current) {
				return { ...previous, owner: ownerAddressInputElement.current.value };
			}
			return previous;
		});
	}

	function useCurrentAddressHandler(e: React.MouseEvent<HTMLElement>) {
		e.preventDefault();
		if (ownerAddressInputElement.current) {
			ownerAddressInputElement.current.value = state.wallet.address;
			addressChangedHandler();
		}
	}

	function ergsAmountChangeHandler() {
		if (ergsAmountInputElement.current) {
			const nanoErgsValue = parseFloat(ergsAmountInputElement.current.value) * NANO_ERGS_IN_ONE_ERG;
			const maxNanoErgs = state.wallet.balance.nanoErgs - (fees.service + fees.blockchain + MIN_NANO_ERGS_IN_BOX);
			if (nanoErgsValue > maxNanoErgs) {
				ergsAmountInputElement.current.value = (maxNanoErgs / NANO_ERGS_IN_ONE_ERG).toString();
			}
			addERGsHandler();
		}
	}

	//TODO let user now that he should first set the ERGs amount...

	function priceChangeHandler() {
		setSettings((previous: ICreateBoxSettings) => {
			if (priceInputElement.current) {
				return { ...previous, price: parseInt(priceInputElement.current.value) };
			} else return previous;
		});
	}

	function addERGsHandler() {
		if (ergsAmountInputElement.current) {
			const nanoErgsValue = parseFloat(ergsAmountInputElement.current.value) * NANO_ERGS_IN_ONE_ERG;
			const maxNanoErgs = state.wallet.balance.nanoErgs - (fees.service + fees.blockchain);
			const valueToSet =
				(nanoErgsValue > MIN_NANO_ERGS_IN_BOX) ?
					(nanoErgsValue > maxNanoErgs) ?
						maxNanoErgs :
						nanoErgsValue
					: MIN_NANO_ERGS_IN_BOX
				;
			setSettings((previous: ICreateBoxSettings) => {
				if (ergsAmountInputElement.current) {
					return { ...previous, ergs: valueToSet };
				}
				return previous;
			});
		}
	}

	function createBabelBoxButtonHandler(e: React.MouseEvent<HTMLElement>) {
		e.preventDefault();

		validate_form(settings, state.wallet.balance.nanoErgs).then((form_valid: boolean) => {
			if (!form_valid) return;
			create_transaction_babel_fee_box_creation(
				settings.owner, settings.price, settings.ergs, props.tokenParams.id
			).then(u_tx => {
				sign_tx(u_tx).then(s_tx => {
					if (s_tx === null) return;
					submit_tx(s_tx).then(res => {
						if (res !== null) {
							props.txSent(res);
						}
					});
				});
			});
		});

	}

	return (
		<div className="create-babel-box-main-div">
			<form action="">
				<label htmlFor="owner_address">Owner address:</label>
				<input type="text" placeholder="Owner's address (yours)" id="owner_address"
					onChange={addressChangedHandler} ref={ownerAddressInputElement}
				/>
				<button onClick={useCurrentAddressHandler} className="create-inheritance-form-button">
					Use current address
				</button>
				<label htmlFor="ergs_amount">ERGs amount:</label>
				<input type="number" name="ergs-amount" min={MIN_NANO_ERGS_IN_BOX / NANO_ERGS_IN_ONE_ERG}
					max={state.wallet.balance.nanoErgs / NANO_ERGS_IN_ONE_ERG} step="0.01"
					onChange={ergsAmountChangeHandler} id="ergs_amount"
					placeholder="ERG amount" ref={ergsAmountInputElement}
				/>
				<label htmlFor="price">Price you are willing to pay for {
					1 * (10 ** (-props.tokenParams.info.decimals))} {props.tokenParams.info.name} in nanoERG:</label>
				<input type="number" name="price" min={1}
					max={settings.ergs} step="1"
					onChange={priceChangeHandler} id="price"
					placeholder="Price" ref={priceInputElement}
				/>
			</form>
			<div>
				<h3>Summary:</h3>
				<table className="create-babel-box-summary">
					<tbody>
						<tr><th>Owner:</th></tr>
						<tr><td>{settings.owner}</td></tr>
						<tr><th>ERGs to put into box:</th></tr>
						<tr><td>{settings.ergs / NANO_ERGS_IN_ONE_ERG} ERG</td></tr>
						<tr><th>Price:</th></tr>
						<tr><td>{settings.price} nanoERG ({
							(settings.price / NANO_ERGS_IN_ONE_ERG)
						} ERG)</td></tr>

					</tbody>
				</table>
			</div>
			<button onClick={createBabelBoxButtonHandler}>Create Babel Fee Box</button>
		</div>
	);

}

export default CreateBabelBoxForm;
