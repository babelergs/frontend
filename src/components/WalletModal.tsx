import React, { Fragment } from "react";

import { AppContext } from "../redux/AppContext";

import { USE_MAINNET } from "../scripts/blockchainParameters";

import { IUTXO, get_total_balance_including_tokens } from "../scripts/walletConnector";

import AssetsSelected from "./AssetsSelected";

import "../css/WalletModal.css";

const NautilusLogo: string = require("../icons/nautilus-logo-icon.svg").default;

declare const ergoConnector: {
	nautilus: {
		connect(settings: { createErgoObject: boolean } | undefined): Promise<boolean>;
		isConnected(): Promise<boolean>;
		disconnect(): Promise<boolean>;
		getContext(): Promise<object>;
	};
};

//todo check whole P2PK address correctly not only it's start!!!
function is_mainnet_p2pk_address(address: string): boolean {
	return address.startsWith("9");
}

//todo check whole P2PK address correctly not only it's start!!!
function is_testnet_p2pk_address(address: string): boolean {
	return address.startsWith("3W");
}

function address_is_on_correct_chain(address: string): boolean{
	return(
		(USE_MAINNET && is_mainnet_p2pk_address(address)) ||
		((!USE_MAINNET) && is_testnet_p2pk_address(address))
	);
}

function get_incorrect_network_warning(mainnet: boolean): string {
	return (
		"Current frontend instance is built for " +
		(mainnet ? "MAINNET" : "TESTNET") +
		" network, but your wallet is using " +
		(mainnet ? "TESTNET" : "MAINNET") +
		" network instead. " +
		"Please use wallet running on correct network."
	);
}

//todo introduce "reload" functionality to reload balances

function WalletModal(props: any) {

	const { state, dispatch } = React.useContext(AppContext);

	function checkIfNautilusAvailable() {
		return (typeof ergoConnector !== "undefined");
	}

	function NautilusConnectHandler() {


		ergoConnector.nautilus.connect({ createErgoObject: false }).then((result) => {

			if (!result) {
				console.log("rejected");
				return;
			}

			dispatch({ type: "wallet_set_connected", payload: true });

			ergoConnector.nautilus.getContext().then((context: any) => {

				context.get_change_address().then((address: string) => {
					if (address_is_on_correct_chain(address)){
						dispatch({ type: "wallet_set_address", payload: address });
					}else{
						disconnectButtonHandler();
						//todo switch to better way of showing warning
						//instead of calling alert
						alert(get_incorrect_network_warning(USE_MAINNET));
					}
				});


				context.get_utxos().then((utxos: IUTXO[]) => {

					get_total_balance_including_tokens(utxos).then((res) => {

						dispatch({ type: "wallet_set_balance", payload: res });
						props.setWalletModalOpened(false);

					});

				});

			});

		});

	}

	function disconnectButtonHandler() {
		dispatch({ type: "wallet_reset" });
		ergoConnector.nautilus.disconnect().then((result) => {
			props.setWalletModalOpened(false);
		});
	}

	return (
		<div className="wallet-modal">
			<div className="wallet-modal-content">
				<span className="wallet-modal-close" onClick={props.closeHandler}>&times;</span>
				{
					!state.wallet.connected ?
						<Fragment>
							<h2>Select a wallet</h2>
							<p>Currently only Nautilus Wallet is supported.</p>
							{checkIfNautilusAvailable() ?
								<button className="wallet-modal-wallet-button" onClick={NautilusConnectHandler}>
									<p>Nautilus Wallet</p>
									<img src={NautilusLogo} alt="" />
								</button>
								: <p>Sorry, It seems that you don't have Nautilus Wallet installed!</p>
							}
						</Fragment>
						:
						<Fragment>
							<h2>Connected wallet:</h2>
							<p>{state.wallet.address}</p>
							<p>Assets:</p>
							<AssetsSelected
								assets={{
									ergs: state.wallet.balance.nanoErgs,
									tokens: state.wallet.balance.tokens
								}}
							/>
							<button className="wallet-modal-disconnect-button" onClick={disconnectButtonHandler}>
								Disconnect wallet
							</button>
						</Fragment>
				}


			</div>
		</div>
	);

}

export default WalletModal;
