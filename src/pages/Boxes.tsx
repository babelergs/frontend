import React, { useState, useEffect } from "react";

import { Link, useSearchParams, Navigate } from "react-router-dom";

import { AppContext } from "../redux/AppContext";

import { ITokenInfo, get_token_info, validate_token_id } from "../scripts/walletConnector";
import { USE_MAINNET } from "../scripts/blockchainParameters";

import TokenIDSelector from "../components/tokens_id_selector/TokenIDSelector";
import TokenInfoDisplay from "../components/TokenInfoDisplay";
import BabelFeeBoxesList from "../components/BabelFeeBoxesList";

export interface ITokenParams {
	loaded: boolean,
	valid: boolean,
	id: string,
	info: ITokenInfo
};

export const TOKEN_PARAMS_DEFAULT_STATE: ITokenParams = {
	loaded: false,
	valid: false,
	id: "",
	info: {
		decimals: 0,
		name: "",
		description: "",
		emissionAmount: 0
	}
};

function BoxesPage() {

	const { state } = React.useContext(AppContext);

	const [tokenParams, setTokenParams] = useState<ITokenParams>(TOKEN_PARAMS_DEFAULT_STATE);
	const [onlyUsersBoxes, setOnlyUsersBoxes] = useState<boolean>(false);

	const [searchParams] = useSearchParams();

	const token_id = searchParams.get("token_id");

	useEffect(() => {
		if (token_id !== null && validate_token_id(token_id) && (!tokenParams.loaded)) {
			get_token_info(token_id).then(
				result => {
					setTokenParams({
						...TOKEN_PARAMS_DEFAULT_STATE, loaded: true, valid: true, id: token_id, info: result
					});
				}
			).catch(
				() => {
					setTokenParams({ ...TOKEN_PARAMS_DEFAULT_STATE, loaded: true, valid: false });
				}
			);
		} else if (token_id === null) {
			setTokenParams(TOKEN_PARAMS_DEFAULT_STATE);
		}
	}, [token_id, tokenParams.loaded]);

	function radioChanged(e: React.ChangeEvent<HTMLInputElement>) {
		setOnlyUsersBoxes(e.target.checked);
	}

	if (token_id === null) {

		return (
			<div>
				<h1>Available Babel Fee boxes overview</h1>
				<p>Here you will be able to see available babel fee boxes for specific token.
					<br />
					Please choose token to see whether there are available some babel fee boxes for it.
				</p>
				<TokenIDSelector />
			</div>
		);

	} else if (!validate_token_id(token_id)) {
		return <Navigate to="/boxes" />;
	} else {
		return (
			<div>
				{
					(!tokenParams.loaded) ? <h2>Loading token info...</h2> :
						(!tokenParams.valid) ?
							<h2>Token with id = {token_id} don't exist on {USE_MAINNET ? "MAINNET" : "TESTNET"}!</h2> :
							(tokenParams.info.emissionAmount === 1) ?
								<h2>There cannot be created babel fee boxes for NFTs</h2> :
								<div>
									<h2>Currently selected token:</h2>
									<p>(To select another token click <Link to="/boxes">here</Link>)</p>
									<TokenInfoDisplay tokenID={token_id} tokenInfo={tokenParams.info} />
									{
										(!state.wallet.connected) ?
											<p>
												In order to be able to create babel fee box for this token, please connect your wallet.
											</p> :
											<p>
												If you want to create babel fee box for this token, click <Link
													to={"/create-box?token_id=" + token_id}>here</Link>.
											</p>
									}
									<h2>Available babel fee boxes for selected token:</h2>
									{
										(!state.wallet.connected) ? [] :
											<label>
												<input
													type="checkbox" name="filter" value="true"
													onChange={radioChanged}
												/>
												Show only boxes belonging to you
											</label>
									}
									<BabelFeeBoxesList
										tokenParams={tokenParams}
										userAddress={state.wallet.connected ? state.wallet.address : ""}
										onlyUsersBoxes={onlyUsersBoxes}
									/>
								</div>
				}
			</div>
		);
	}


}

export default BoxesPage;
