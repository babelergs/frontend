import React from "react";

import { Link } from "react-router-dom";

import "../css/About.css";

function AboutPage() {

	return (
		<div>
			<div className="about-page-text">

				<h1>About</h1>

				<h2>What is BabelERGs project about?</h2>
				<p>BabelERGs is liquidity managment solution for <a href="https://github.com/nitram147/eips/blob/eip28/eip-0031.md" target="_blank" rel="noopener norefferer">Babel fees</a> on Ergo blockchain.</p>

				<h2>And what are the "Babel fees"?</h2>
				<p>Babel fees are a concept of paying transaction fees in tokens (e.g. stablecoins) instead of the platform's primary token (ERG). For more information about the origin of the term “babel fees“, please see the following IOHK article: <a href="https://iohk.io/en/blog/posts/2021/02/25/babel-fees/" target="_blank" rel="noopener norefferer">https://iohk.io/en/blog/posts/2021/02/25/babel-fees/</a>.</p>

				<h2>What is the difference between Cardano's babel fees and Ergo's babel fees?</h2>
				<p>Cardano's babel fees are still only a concept, while Ergo's babel fees are already specified by the Ergo Improvement proposal <a href="https://github.com/nitram147/eips/blob/eip28/eip-0031.md" target="_blank" rel="noopener norefferer">EIP&#8209;0031</a>, and are currently being implemented.</p>
				<p>EIP&#8209;0031 aims to provide the standard for paying fees in tokens, and thus has the same goal as Cardano’s “babel fees“, however, it chooses a different approach, with the main difference being that EIP&#8209;0031 does not require any type of forking.</p>
				<p>With the Cardano’s approach, user publishes “invalid"(incomplete) transaction and has to wait, hoping that somebody will take his tokens and pay the transaction fees in a primary token (ADA), therefore completing the transaction. EIP&#8209;0031, on the other hand, chooses the opposite approach.</p>
				<p>Supporters who wish to make money out of EIP&#8209;0031 will publish UTXOs, containing primary tokens locked by smartcontract. These will contain price attribute (i.e. how much of the primary tokens is that one specific supporter willing to pay for one piece of user’s tokens (fe. stablecoins)). Let’s call this user’s token a “babel token”.</p>
				<p>User who is willing to pay the transaction fee in babel tokens can now find whether there exist any UTXOs belonging to the P2S address specified by the corresponding smartcontract for that specific babel token. If there is any UTXO which contains enough primary tokens for required fees, the user can calculate the price of buying the required amount of primary tokens from this UTXO and then decide whether or not he wishes to use it. In case he accepts this exchange ratio (defined by the UTXO’s price attribute), he can consequently spend this UTXO in his transaction to cover the transaction fees. This spending user now has to recreate the UTXO with the same parameters and insert the required amount of babel tokens into it (primary tokens difference should be less or equal to inserted babel tokens amount times price), which is going to be ensured by the smartcontract.</p>
				<p>Strong advantage of this approach (compared to Cardano’s one) is that user always knows in advance whether there is an opportunity to pay via “babel fees” or not, and if there is, what is the exchange ratio. He can therefore be (almost) certain that if he decides to use it, his transaction will be included in the blockchain ledger. Be aware, however, that there exist some exceptions to this rule, which is later discussed in the “Wallets implementation” section.</p>

				<h2>Motivation</h2>
				<p>Many users use blockchain solely for transferring (native)tokens, such as stablecoins, or even “meme” coins. These users, understandably, do not want to be bothered with keeping an eye on the amount of blockchain’s primary token they own, or even obtaining this primary token in the first place.</p>
				<p>Once they run out of primary token, they have to difficultly (and often costly), swap their tokens of interest for the primary tokens, that they can later use to cover transaction fees. Since primary tokens are also needed for these swaps, users may be forced to introduce new capital to their portfolio solely for the purpose of purchasing primary tokens, used for fee paying.</p>
				<p>Since basic transactional fees on the Ergo blockchain are generally quite low, the "babel fees" users would be probaby willing to pay a fee that could be higher than that of a primary token transaction, in exchange for being able to pay in their token of interest and not having to bother with the blockchain’s primary token purchase.</p>
				<p>This brings up a financial incentive for “EIP&#8209;0031 supporters”, who could benefit out of this arbitrage by providing the liquidity for such “babel fees” users, with primary token’s selling price (expressed in tokens of interest) being higher compared to the same token pair on the exchanges</p>

				<h2>Became an "EIP&#8209;0031 supporter" by providing liquidity via <Link to="/how-it-works">BabelERGs</Link> ;-)</h2>

			</div>
		</div>
	);

}

export default AboutPage;
