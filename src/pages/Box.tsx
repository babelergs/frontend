import React, { useState, useEffect } from "react";

import { Navigate, Link, useSearchParams, useNavigate } from "react-router-dom";

import { AppContext } from "../redux/AppContext";

import { IBabelBox, retrieve_babel_box_by_id } from "../scripts/babelFeeBoxTools";
import { get_token_info, validate_token_id } from "../scripts/walletConnector";

import { ITokenParams, TOKEN_PARAMS_DEFAULT_STATE } from "./Boxes";

import BabelFeeBoxItem from "../components/BabelFeeBoxItem";
import FullWithdrawalForm from "../components/withdrawal_forms/FullWithdrawalForm";
import PartialWithdrawalOrChangePriceForm from "../components/withdrawal_forms/PartialWithdrawalOrChangePriceForm";

//TODO show somewhere also earned tokens value...
//(implement it once there will be testable babel fees integration in wallet so we can collect some real babel fees)

function BoxPage() {

	const { state } = React.useContext(AppContext);

	const [babelBox, setBabelBox] = useState<IBabelBox>({
		boxId: "",
		price: 0,
		owner: "",
		nanoERGs: 0,
		tokenId: "",
		tokens: []
	});

	const [tokenParams, setTokenParams] = useState<ITokenParams>(TOKEN_PARAMS_DEFAULT_STATE);

	const [txId, setTxId] = useState<string>("");

	const [searchParams] = useSearchParams();

	const box_id = searchParams.get("box_id");

	const navigate = useNavigate();

	useEffect(() => {
		if (box_id !== null) {
			retrieve_babel_box_by_id(box_id).then(box => {
				setBabelBox(box);
			}).catch(() => navigate("/boxes"));
		}
	}, [box_id, navigate]);

	useEffect(() => {
		if (babelBox.tokenId !== "" && validate_token_id(babelBox.tokenId) && (!tokenParams.loaded)) {
			get_token_info(babelBox.tokenId).then(
				result => {
					setTokenParams({
						...TOKEN_PARAMS_DEFAULT_STATE, loaded: true, valid: true, id: babelBox.tokenId, info: result
					});
				}
			).catch(
				() => {
					setTokenParams({ ...TOKEN_PARAMS_DEFAULT_STATE, loaded: true, valid: false });
				}
			);
		} else if (babelBox.tokenId === "") {
			setTokenParams(TOKEN_PARAMS_DEFAULT_STATE);
		}
	}, [babelBox.tokenId, tokenParams.loaded]);


	if (!state.wallet.connected) {
		return <Navigate to="/" />
	}

	if (box_id === null) {
		return <Navigate to="/boxes" />;
	}

	if (babelBox.boxId !== "" && babelBox.owner !== state.wallet.address) {
		return <Navigate to="/boxes" />;
	}

	if (txId !== "") {
		return (
			<div>
				<h3>Transaction&nbsp;
					<a href={"https://testnet.ergoplatform.com/en/transactions/" + txId}
						target="_blank" rel="noopener norefferer">
						{txId}
					</a>
					&nbsp;was successfully sent! :-)</h3>
			</div>
		);
	}

	return (
		<div>
			{
				(babelBox.boxId === "") ? <h2>Loading babel box...</h2> :
					<div>
						<h1>Babel fee box overview</h1>
						<p>
							(See other babel fee boxes for {tokenParams.info.name} token <Link
								to={"/boxes?token_id=" + babelBox.tokenId}>here</Link>
							)</p>
						<BabelFeeBoxItem
							babelFeeBox={babelBox}
							tokenParams={tokenParams}
							showLink={false}
						/>
						<h2>Full withdrawal:</h2>
						<FullWithdrawalForm boxId={babelBox.boxId} setTxSent={setTxId} />
						<h2>Partial withdrawal or price change:</h2>
						<PartialWithdrawalOrChangePriceForm />
					</div>
			}
		</div>
	);

}

export default BoxPage;
