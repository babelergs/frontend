import React from "react";

import { Link } from "react-router-dom";

import "../css/Home.css";

function HomePage() {

	return (
		<div className="home-page-div">
			<img src="./babelergs_logo.svg" alt="BabelERGs - Babel Fees liquidity managment"
				title="BabelERGs - Babel Fees liquidity managment" />
			<h1>Manage the liquidity of Babel Fees</h1>
			<Link to="/how-it-works">
				<button>See how it works!</button>
			</Link>
		</div>
	);

}

export default HomePage;
