import React, { useState, useEffect } from "react";

import { Navigate, useSearchParams, Link } from "react-router-dom";

import { AppContext } from "../redux/AppContext";

import { get_token_info, validate_token_id } from "../scripts/walletConnector";
import { USE_MAINNET } from "../scripts/blockchainParameters";

import { ITokenParams, TOKEN_PARAMS_DEFAULT_STATE } from "./Boxes";

import TokenInfoDisplay from "../components/TokenInfoDisplay";
import CreateBabelBoxForm from "../components/CreateBabelBoxForm";

function CreateBoxPage() {

	const { state } = React.useContext(AppContext);

	const [tokenParams, setTokenParams] = useState<ITokenParams>(TOKEN_PARAMS_DEFAULT_STATE);
	const [txId, setTxId] = useState<string>("");

	const [searchParams] = useSearchParams();

	const token_id = searchParams.get("token_id");

	useEffect(() => {
		if (token_id !== null && validate_token_id(token_id) && (!tokenParams.loaded)) {
			get_token_info(token_id).then(
				result => {
					setTokenParams({
						...TOKEN_PARAMS_DEFAULT_STATE, loaded: true, valid: true, id: token_id, info: result
					});
				}
			).catch(
				() => {
					setTokenParams({ ...TOKEN_PARAMS_DEFAULT_STATE, loaded: true, valid: false });
				}
			);
		}
	});

	//TODO get rid of this non-necessary function and implement it like in Box.tsx in FullWithdrawal module...
	function txSent(txId: string) {
		setTxId(txId);
	}

	if (!state.wallet.connected) {
		return <Navigate to="/boxes" />
	}

	if (token_id === null || (!validate_token_id(token_id))) {
		return <Navigate to="/boxes" />;
	}

	if (txId !== "") {
		return (
			<div>
				<h3>Transaction&nbsp;
					<a href={"https://testnet.ergoplatform.com/en/transactions/" + txId}
						target="_blank" rel="noopener norefferer">
						{txId}
					</a>
					&nbsp;was successfully sent! :-)</h3>
			</div>
		);
	}

	return (
		<div>
			<h1>Create new babel fee box</h1>
			{
				(!tokenParams.loaded) ? <h2>Loading token info...</h2> :
					(!tokenParams.valid) ?
						<h2>Token with id = {token_id} don't exist on {USE_MAINNET ? "MAINNET" : "TESTNET"}!</h2> :
						(tokenParams.info.emissionAmount === 1) ?
							<h2>There cannot be created babel fee boxes for NFTs</h2> :
							<div>
								<h2>Currently selected token:</h2>
								<p>(To select another token click <Link to="/boxes">here</Link>)</p>
								<TokenInfoDisplay tokenID={token_id} tokenInfo={tokenParams.info} />
								<h2>Please fill following form in order to create your babel box:</h2>
								<CreateBabelBoxForm tokenParams={tokenParams} txSent={txSent} />
							</div>
			}
		</div>
	);

}

export default CreateBoxPage;
