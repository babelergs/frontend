import React from "react";

import "../css/HowItWorks.css";

function HowItWorksPage() {

	return (
		<div>
			<h1>How it works?</h1>
			<p>Go to "Boxes" section. There you will be able to select a token of your interest. There are 2 options for "observing user" and 3 options for user with connected wallet:</p>
			<a href="how-to-images/1.png" target="_blank"><img src="how-to-images/1.png" className="how-to-image" alt="" /></a>
			<a href="how-to-images/2.png" target="_blank"><img src="how-to-images/2.png" className="how-to-image" alt="" /></a>
			<p>Well known tokens option let you choose from "well known" or "verified" tokens e.g. SigmaUSD, ErgoPAD etc.</p>
			<p>You can also choose from tokens that you have in your balance at Nautilus wallet.</p>
			<p>Or you can just enter Token ID manually</p>
			<a href="how-to-images/3.png" target="_blank"><img src="how-to-images/3.png" className="how-to-image" alt="" /></a>
			<p>Once you have selected your token, you will be able to see token info together with list of available babel fee boxes:</p>
			<p>(This token don't have any available babel fee boxes)</p>
			<a href="how-to-images/4.png" target="_blank"><img src="how-to-images/4.png" className="how-to-image" alt="" /></a>
			<p>And this one have:</p>
			<a href="how-to-images/5.png" target="_blank"><img src="how-to-images/5.png" className="how-to-image" alt="" /></a>
			<p>After clicking on "here" in the create babel fee box for this token message, you will abble to do so:</p>
			<p>Just set babel fee box owner, with how many ERGs you want to provide liquidity with and the price.</p>
			<p>Price means how many nanoERGs are you willing to pay for one babel fee token. <br />(By number "one" we mean lowest possible denomination e.g. 0.01 in case of token with 2 decimal places.)</p>
			<a href="how-to-images/6.png" target="_blank"><img src="how-to-images/6.png" className="how-to-image" alt="" /></a>
			<p>As we can see, our babel fee box was sucessfully created B-)</p>
			<a href="how-to-images/7.png" target="_blank"><img src="how-to-images/7.png" className="how-to-image" alt="" /></a>
			<p>Clicking on it's ID will redirect to babel fee box management page.</p>
			<p>At this page you will be able to do either full withdrawal (destroying babel fee box and withdrawing all the funds from it),</p>
			<p>or you will be able (currently it's not implemented but can be "emulated" - see text in the picture) to do partial withdrawal or price change or their combination :-)</p>
			<a href="how-to-images/8.png" target="_blank"><img src="how-to-images/8.png" className="how-to-image" alt="" /></a>
		</div>
	);

}

export default HowItWorksPage;
