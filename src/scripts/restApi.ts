function handle_errors(response: Response): Response {
	if (!response.ok) {
		throw new Error("API returned error code!");
	}
	return response;
}

export async function get_request(url: string) {
	const request = await fetch(url).catch(
		() => { throw new Error("GET request failed!"); }
	);
	const handled_request = handle_errors(request);
	const result = await handled_request.json();
	return result;
}

export async function post_request(url: string, body: string) {

	const request = await fetch(url, {
		method: 'POST',
		body,
		headers: {
			'Content-Type': 'application/json',
		}
	}).catch(() => { throw new Error("POST request failed!"); });

	const handled_request = handle_errors(request);
	const result = await handled_request.json();

	return result;
}
