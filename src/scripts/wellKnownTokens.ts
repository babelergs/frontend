import { USE_MAINNET } from "./blockchainParameters";

export interface IWellKnownToken {
	id: string,
	name: string
	decimals: number,
	icon: string
};

export const wellKnownTokensListMainnet: IWellKnownToken[] = [];
export const wellKnownTokensListTestnet: IWellKnownToken[] = [
	{
		id: "4f530d4875e64b9ef9ec2dfffee69ad2bd9bd9b6aadcce9a7258acacd8aeb09d",
		name: "FirstTKN",
		decimals: 2,
		icon: "todo"
	},
	{
		id: "1cfa63a6be12cc15e1d106ad0b96383533ac6dc610869eca9c76a6c6a43013f4",
		name: "SecondTKN",
		decimals: 0,
		icon: "todo"
	}
];

export const wellKnownTokensList: IWellKnownToken[] =
	USE_MAINNET ? wellKnownTokensListMainnet : wellKnownTokensListTestnet;
