import {
	EXPLORER_URL, EXPLORER_BALANCE_ENDPOINT_PREFIX, EXPLORER_BALANCE_ENDPOINT_SUFIX,
	EXPLORER_TOKEN_INFO_PREFIX, EXPLORER_BOX_INFO_PREFIX
} from "./blockchainParameters";

import { get_request } from "./restApi";

export interface IToken {
	id: string,
	amount: number,
	decimals: number,
	name: string,
	description: string
}

//todo add "loaded" property to balance
export interface IBalanceWithTokens {
	nanoErgs: number,
	tokens: IToken[]
}

export interface IWalletProperties {
	connected: boolean;
	address: string;
	balance: IBalanceWithTokens;
}

export const WALLET_DEFAULT_STATE: IWalletProperties = {
	connected: false,
	address: "",
	balance: {
		nanoErgs: 0,
		tokens: []
	}
};

export interface IUTXOToken {
	tokenId: string,
	amount: string
}

export interface IUTXO {
	boxId: string,
	transactionId: string,
	index: number,
	ergoTree: string,
	creationHeight: number,
	value: string,
	assets: IUTXOToken[],
	additionalRegisters: any,
	confirmed: boolean
};

export async function get_address_balance(address: string): Promise<number> {

	const response = await get_request(EXPLORER_URL +
		EXPLORER_BALANCE_ENDPOINT_PREFIX +
		address +
		EXPLORER_BALANCE_ENDPOINT_SUFIX
	);

	return response.confirmed.nanoErgs;

}

export interface ITokenInfo {
	decimals: number,
	name: string,
	description: string,
	emissionAmount: number
}

export async function get_token_info(id: string): Promise<ITokenInfo> {

	const response = await get_request(EXPLORER_URL + EXPLORER_TOKEN_INFO_PREFIX + id).catch(
		() => { throw new Error("Token info retrieval failed!"); }
	);

	const token_info: ITokenInfo = {
		decimals: response.decimals,
		name: response.name,
		description: response.description,
		emissionAmount: response.emissionAmount
	};

	return token_info;
}

export interface IExplorerUTXOToken {
	tokenId: string,
	index: number,
	amount: number,
	name: string | null,
	decimals: number | null,
	type: string | null
}

export interface IExplorerUTXORegister{
	serializedValue: string,
	sigmaType: string,
	renderedValue: string
}

export interface IExplorerUTXORegisters {
	R4?: IExplorerUTXORegister,
	R5?: IExplorerUTXORegister,
	R6?: IExplorerUTXORegister,
	R7?: IExplorerUTXORegister,
	R8?: IExplorerUTXORegister,
	R9?: IExplorerUTXORegister
}

export interface IExplorerUTXO {
	boxId: string,
	transactionId: string,
	blockId: string,
	value: number,
	index: number,
	globalIndex: number,
	creationHeight: number,
	settlementHeight: number,
	ergoTree: string,
	address: string,
	assets: IExplorerUTXOToken[],
	additionalRegisters: IExplorerUTXORegisters,
	spentTransactionId: string | null,
	mainChain: boolean
}

export async function get_box_to_spend(id: string): Promise<IUTXO> {

	const response: IExplorerUTXO = await get_request(EXPLORER_URL + EXPLORER_BOX_INFO_PREFIX + id);

	let utxo: IUTXO = {
		...response, value: response.value.toString(), assets: [], confirmed: true
	};

	response.assets.forEach((token: IExplorerUTXOToken) => {
		utxo = {
			...utxo, assets: [...utxo.assets, {
				tokenId: token.tokenId,
				// index: token.index,
				amount: token.amount.toString(),
				// name: token.name,
				// decimals: token.decimals,
				// type: token.type
			}]
		}
	});

	return utxo;
}

export async function get_box_to_spend_original_json(id: string): Promise<string> {
	const response: IExplorerUTXO = await get_request(EXPLORER_URL + EXPLORER_BOX_INFO_PREFIX + id);
	return JSON.stringify(response);
}

export async function get_box_balance(id: string, skip_token: string): Promise<IBalanceWithTokens> {

	const response = await get_request(EXPLORER_URL + EXPLORER_BOX_INFO_PREFIX + id);

	const box_balance: IBalanceWithTokens = {
		nanoErgs: response.value,
		tokens: []
	}

	for (var i = 0; i < response.assets.length; ++i) {
		const token = response.assets[i];
		if (token.tokenId === skip_token) continue;
		const token_info = await get_token_info(token.tokenId);
		box_balance.tokens = [...box_balance.tokens,
		{
			id: token.tokenId,
			amount: parseInt(token.amount),
			decimals: token_info.decimals,
			name: token_info.name,
			description: token_info.description
		}
		];
	}

	return box_balance;
}

export async function get_total_balance_including_tokens(utxos: IUTXO[]): Promise<IBalanceWithTokens> {
	const accumulator: IBalanceWithTokens = { nanoErgs: 0, tokens: [] };
	for (var i = 0; i < utxos.length; ++i) {
		const utxo = utxos[i];
		accumulator.nanoErgs += parseInt(utxo.value);
		for (var j = 0; j < utxo.assets.length; ++j) {
			const token = utxo.assets[j];
			if (!accumulator.tokens.find(tkn => (tkn.id === token.tokenId))) {
				const token_info = await get_token_info(token.tokenId);
				accumulator.tokens = [...accumulator.tokens,
				{
					id: token.tokenId,
					amount: parseInt(token.amount),
					decimals: token_info.decimals,
					name: token_info.name,
					description: token_info.description
				}
				];
			} else {
				const tmp_tkn: IToken | undefined = accumulator.tokens.find(tkn => (tkn.id === token.tokenId));
				if (tmp_tkn) {
					tmp_tkn.amount += parseInt(token.amount);
				}
			}
		}
	}
	return accumulator;
}

export async function is_nft(nft: string): Promise<boolean> {
	const response = await get_request(EXPLORER_URL + EXPLORER_TOKEN_INFO_PREFIX + nft);
	if (response.status === 404 || response.emissionAmount !== 1) return false;
	return true;
}

export function validate_token_id(tokenID: string): boolean {

	if (tokenID.length !== 64) return false;

	function valid_hex_value(str: string): boolean {
		const n = str.length;
		for (let i = 0; i < n; ++i) {
			const ch = str[i];
			if ((ch < '0' || ch > '9') &&
				(ch < 'A' || ch > 'F') &&
				(ch < 'a' || ch > 'f')
			) {
				return false;
			}
		}
		return true;
	}

	return valid_hex_value(tokenID);
}
