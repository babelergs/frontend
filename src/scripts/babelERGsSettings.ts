import { USE_MAINNET } from "./blockchainParameters";

export const BABELERGS_SERVICE_FEE_AMOUNT: number = 100000000;

export const MAINNET_SERVICE_FEE_ADDRESS: string = "todo";
export const TESTNET_SERVICE_FEE_ADDRESS: string = "3WwXjy6WHJM4DXRvkn4gZcWuC5wLYimikxKeuKnkEEsCBx3JSegy";

export const BABELERGS_SERVICE_FEE_ADDRESS: string =
	(USE_MAINNET) ? MAINNET_SERVICE_FEE_ADDRESS : TESTNET_SERVICE_FEE_ADDRESS;
