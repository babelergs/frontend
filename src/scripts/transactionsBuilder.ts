import { get_request } from "./restApi";
import {
	EXPLORER_URL, EXPLORER_BLOKCHAIN_HEIGHT_PREFIX, MIN_NANO_ERGS_IN_BOX,
	USE_MAINNET, ADDRESS_NETWORK_TYPE_CURRENT, TRANSACTION_CREATE_BABEL_BOX_FEE,
	TRANSACTION_OWNER_WITHDRAWAL_BLOCKCHAIN_FEE
} from "./blockchainParameters";
import { BABELERGS_SERVICE_FEE_ADDRESS, BABELERGS_SERVICE_FEE_AMOUNT } from "../scripts/babelERGsSettings";
import { IUTXOToken } from "./walletConnector";

import {
	generate_babel_fee_address_for_token
} from "../smartcontracts/smartcontractsGenerator";

let ergolib = import("ergo-lib-wasm-browser");

declare const ergoConnector: {
	nautilus: {
		connect(settings: { createErgoObject: boolean } | undefined): Promise<boolean>;
		isConnected(): Promise<boolean>;
		disconnect(): Promise<boolean>;
		getContext(): Promise<object>;
	};
};

interface ITxInput {
	boxId: string,
	extension: any
}

interface ITxOutputAsset {
	tokenId: string,
	amount: number
}

interface ITxOutputAssetConverted {
	tokenId: string,
	amount: string
}

interface ITxOutput {
	value: number,
	ergoTree: string,
	assets: ITxOutputAsset[],
	additionalRegisters: any,
	creationHeight: number
}

interface ITxOutputConverted {
	value: string,
	ergoTree: string,
	assets: ITxOutputAssetConverted[],
	additionalRegisters: any,
	creationHeight: number
}

interface ITx {
	inputs: ITxInput[],
	dataInputs: any[],
	outputs: ITxOutput[]
}

interface ITxConverted {
	inputs: ITxInput[],
	dataInputs: any[],
	outputs: ITxOutputConverted[]
}

interface ISignedTxInput {
	boxId: string,
	spendingProof: {
		proofBytes: string,
		extension: any
	}
}

interface ISignedOutput {
	boxId: string,
	value: number,
	ergoTree: string,
	assets: ITxOutputAsset[],
	additionalRegisters: any,
	creationHeight: number,
	transactionId: string,
	index: number
}

interface ISignedTx {
	id: string,
	inputs: ISignedTxInput[]
	dataInputs: any[],
	outputs: ISignedOutput[]
}

export function convert_tx_values_number_to_string(tx: ITx): ITxConverted {
	return {
		...tx, outputs: tx.outputs.map(
			(output: ITxOutput) => convert_utxo_values_number_to_string(output))
	};
}

export function convert_utxo_values_number_to_string(json: ITxOutput): ITxOutputConverted {
	if (json.assets == null) {
		json.assets = [];
	}
	return {
		...json,
		value: json.value.toString(),

		assets: json.assets.map((asset: ITxOutputAsset) => ({
			tokenId: asset.tokenId,
			amount: asset.amount.toString(),
		})),
	};
}

export async function validate_address(address: string): Promise<boolean> {

	let wasm = (await ergolib);

	try {
		if (USE_MAINNET) {
			wasm.Address.from_mainnet_str(address);
		} else {
			wasm.Address.from_testnet_str(address);
		}
	} catch (_) { return false; }

	return true;
}

export async function get_block_height(): Promise<number> {
	const response = await get_request(EXPLORER_URL + EXPLORER_BLOKCHAIN_HEIGHT_PREFIX);
	return response.items[0].height;
}

export interface INautilusUTXOAsset {
	tokenId: string,
	amount: string
}

export interface INautilusUTXO {
	boxId: string,
	transactionId: string,
	index: number,
	ergoTree: string,
	creationHeight: number,
	value: string,
	assets: INautilusUTXOAsset[]
	additionalRegisters: any,
	confirmed: boolean
}

async function get_utxos() {
	const ctx: any = await ergoConnector.nautilus.getContext();
	const utxos: INautilusUTXO[] = await ctx.get_utxos();
	return utxos;
}

async function get_box_selection(utxos: any[], nanoErgs: number, tokens: IUTXOToken[]) {

	let wasm = (await ergolib);

	let wasm_tokens = new wasm.Tokens();
	tokens.forEach((token: IUTXOToken) => {
		wasm_tokens.add(
			new wasm.Token(
				wasm.TokenId.from_str(token.tokenId),
				wasm.TokenAmount.from_i64(wasm.I64.from_str(token.amount))
			)
		);
	});

	const selector = new wasm.SimpleBoxSelector();

	return selector.select(
		wasm.ErgoBoxes.from_boxes_json(utxos),
		wasm.BoxValue.from_i64(wasm.I64.from_str(nanoErgs.toString())),
		wasm_tokens
	);

}

async function get_output_box_candidates_babel_fee_box_creation(
	owner: string, price: number, nanoErgs: number, tokenID: string, height: number
) {

	let wasm = (await ergolib);

	const babel_fee_address = await generate_babel_fee_address_for_token(tokenID);

	const service_fee_value = wasm.BoxValue.from_i64(wasm.I64.from_str((BABELERGS_SERVICE_FEE_AMOUNT).toString()));
	const babel_fee_box_erg_value = wasm.BoxValue.from_i64(wasm.I64.from_str(nanoErgs.toString()));

	const output_box_candidates = wasm.ErgoBoxCandidates.empty();

	const first_output_builder = new wasm.ErgoBoxCandidateBuilder(
		babel_fee_box_erg_value,
		wasm.Contract.pay_to_address(wasm.Address.from_base58(babel_fee_address)),
		height
	);

	const second_output_builder = new wasm.ErgoBoxCandidateBuilder(
		service_fee_value,
		wasm.Contract.pay_to_address(wasm.Address.from_base58(BABELERGS_SERVICE_FEE_ADDRESS)),
		height
	);

	//first_output - R4[SigmaProp]
	const ownerSigmaProp = wasm.Constant.from_ecpoint_bytes(
		wasm.Address.from_base58(owner).to_bytes(ADDRESS_NETWORK_TYPE_CURRENT).subarray(1, 34)
	);

	//first_output - R5[Long]
	const priceValue = wasm.Constant.from_i64(
		wasm.I64.from_str(price.toString())
	);

	first_output_builder.set_register_value(4, ownerSigmaProp);
	first_output_builder.set_register_value(5, priceValue);

	output_box_candidates.add(first_output_builder.build());
	output_box_candidates.add(second_output_builder.build());

	return output_box_candidates;
}

async function get_output_box_candidates_withdrawal(
	address: string, nanoErgs: number, tokens: IUTXOToken[], height: number
) {

	let wasm = (await ergolib);

	const service_fee_value = wasm.BoxValue.from_i64(wasm.I64.from_str((BABELERGS_SERVICE_FEE_AMOUNT).toString()));

	const remaining_erg_value = wasm.BoxValue.from_i64(
		wasm.I64.from_str(
			(nanoErgs - TRANSACTION_OWNER_WITHDRAWAL_BLOCKCHAIN_FEE - BABELERGS_SERVICE_FEE_AMOUNT).toString()
		)
	);

	const output_box_candidates = wasm.ErgoBoxCandidates.empty();

	const first_output_builder = new wasm.ErgoBoxCandidateBuilder(
		remaining_erg_value,
		wasm.Contract.pay_to_address(wasm.Address.from_base58(address)),
		height
	);

	tokens.forEach((token: IUTXOToken) => {
		first_output_builder.add_token(
			wasm.TokenId.from_str(token.tokenId),
			wasm.TokenAmount.from_i64(wasm.I64.from_str(token.amount))
		);
	});

	const second_output_builder = new wasm.ErgoBoxCandidateBuilder(
		service_fee_value,
		wasm.Contract.pay_to_address(wasm.Address.from_base58(BABELERGS_SERVICE_FEE_ADDRESS)),
		height
	);

	output_box_candidates.add(first_output_builder.build());
	output_box_candidates.add(second_output_builder.build());

	return output_box_candidates;
}

export async function create_transaction_babel_fee_box_creation(
	owner: string, price: number, nanoErgs: number, tokenID: string
): Promise<ITxConverted> {

	let wasm = (await ergolib);

	const height = await get_block_height();

	const utxos = await get_utxos();
	const input_box_selection = await get_box_selection(
		utxos,
		(nanoErgs + BABELERGS_SERVICE_FEE_AMOUNT + TRANSACTION_CREATE_BABEL_BOX_FEE),
		[]
	);

	const output_box_candidates = await get_output_box_candidates_babel_fee_box_creation(
		owner, price, nanoErgs, tokenID, height
	);

	const miner_fee_value = wasm.BoxValue.from_i64(wasm.I64.from_str((TRANSACTION_CREATE_BABEL_BOX_FEE).toString()));
	const min_change_value = wasm.BoxValue.from_i64(wasm.I64.from_str((MIN_NANO_ERGS_IN_BOX).toString()));

	const tx_builder = wasm.TxBuilder.new(
		input_box_selection,
		output_box_candidates,
		height,
		miner_fee_value,
		wasm.Address.from_base58(owner),
		min_change_value
	);

	const data_inputs = new wasm.DataInputs();
	tx_builder.set_data_inputs(data_inputs);

	const tx = tx_builder.build().to_json();
	const converted = convert_tx_values_number_to_string(JSON.parse(tx));

	converted.inputs = converted.inputs.map((box: any) => {
		const full_box = utxos.find(utxo => utxo.boxId === box.boxId);
		if (full_box)
			return { ...full_box, extension: {} };
		else return box;
	});

	return converted;
}

export async function create_withdrawal_transaction(
	address: string, box_to_spend: INautilusUTXO, nanoErgs: number, tokens: IUTXOToken[]
): Promise<ITxConverted> {

	let wasm = (await ergolib);

	const height = await get_block_height();

	//TODO FIXME - find whether there is or not enough money for such fees (see comment on line)
	const utxos = [box_to_spend]; //assuming that there's enough ERGs in box to be able to pay for transaction + service fee
	const input_box_selection = await get_box_selection(
		utxos,
		nanoErgs,
		tokens
	);

	const output_box_candidates = await get_output_box_candidates_withdrawal(
		address, nanoErgs, tokens, height
	);

	const miner_fee_value = wasm.BoxValue.from_i64(
		wasm.I64.from_str((TRANSACTION_OWNER_WITHDRAWAL_BLOCKCHAIN_FEE).toString())
	);

	const min_change_value = wasm.BoxValue.from_i64(wasm.I64.from_str((MIN_NANO_ERGS_IN_BOX).toString()));

	const tx_builder = wasm.TxBuilder.new(
		input_box_selection,
		output_box_candidates,
		height,
		miner_fee_value,
		wasm.Address.from_base58(address),
		min_change_value
	);

	const data_inputs = new wasm.DataInputs();
	tx_builder.set_data_inputs(data_inputs);

	const tx = tx_builder.build().to_json();
	const converted = convert_tx_values_number_to_string(JSON.parse(tx));

	converted.inputs = converted.inputs.map((box: any) => {
		const full_box = utxos.find(utxo => utxo.boxId === box.boxId);
		if (full_box)
			return { ...full_box, extension: {} };
		else return box;
	});

	return converted;
}

export async function sign_tx(unsigned_tx: ITxConverted): Promise<ISignedTx | null> {
	const ctx: any = await ergoConnector.nautilus.getContext();
	try {
		return ctx.sign_tx(unsigned_tx);
	} catch (_) { return null; }
}

export async function submit_tx(signed_tx: ISignedTx): Promise<string | null> {
	const ctx: any = await ergoConnector.nautilus.getContext();
	try {
		return ctx.submit_tx(signed_tx);
	} catch (_) { return null; }
}

export async function test_it() {

}

export { };
