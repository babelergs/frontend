import { get_request } from "./restApi";

import { EXPLORER_URL, EXPLORER_UNSPENT_BOXES_BY_ADDRESS_PREFIX, EXPLORER_BOX_BY_ID_PREFIX } from "./blockchainParameters";

import {
	generate_babel_fee_ergo_tree_for_token, generate_babel_fee_address_for_token, babel_fee_sigma_prop_to_address
} from "../smartcontracts/smartcontractsGenerator";

import { babelFeesContractErgoTreePrefix, babelFeesContractErgoTreeSuffix } from "../smartcontracts/babelFeesContract";

import { IExplorerUTXOToken, IExplorerUTXO, validate_token_id } from "./walletConnector";

export interface IBabelBox {
	boxId: string,
	price: number,
	owner: string,
	nanoERGs: number,
	tokenId: string,
	tokens: IExplorerUTXOToken[]
}

function extract_token_id_from_ergo_tree(ergoTree: string): string {

	if (ergoTree.length !== (
		babelFeesContractErgoTreePrefix.length + 64 + babelFeesContractErgoTreeSuffix.length
	)) throw new Error("Ergo tree provided to extract_token_id_from_ergo_tree has invalid length!");

	if (
		(!(ergoTree.startsWith(babelFeesContractErgoTreePrefix))) ||
		(!(ergoTree.endsWith(babelFeesContractErgoTreeSuffix)))
	) throw new Error("Ergo tree provided to extract_token_id_from_ergo_tree is not a babel fee box contract!");

	return ergoTree.substr(babelFeesContractErgoTreePrefix.length, 64);
}


//TODO fix typo - validate_babe_fee_box -> validate_babel_fee_box

export function validate_babe_fee_box(tokenID: string, babelFeeBox: IExplorerUTXO): boolean {

	if (!validate_token_id(tokenID)) {
		throw new Error("Invalid token ID provided to validate_babe_fee_box function!");
	}

	const correctErgoTree = generate_babel_fee_ergo_tree_for_token(tokenID);

	if (
		(babelFeeBox.ergoTree !== correctErgoTree) ||
		(!babelFeeBox.additionalRegisters.R4) || (babelFeeBox.additionalRegisters.R4.sigmaType !== "SSigmaProp") ||
		(!babelFeeBox.additionalRegisters.R5) || (babelFeeBox.additionalRegisters.R5.sigmaType !== "SLong")
	) return false;

	return true;
}

export async function construct_babel_fee_boxes_list(tokenID: string): Promise<IBabelBox[]> {

	if (!validate_token_id(tokenID)) {
		throw new Error("Invalid token ID provided to construct_babel_fee_boxes_list function!");
	}

	const babelFeeContractAddress = await generate_babel_fee_address_for_token(tokenID);

	const response = await get_request(
		EXPLORER_URL + EXPLORER_UNSPENT_BOXES_BY_ADDRESS_PREFIX + babelFeeContractAddress
	).catch(() => { throw new Error("Fee boxes retrieval failed!"); });

	const result: IBabelBox[] = [];

	for (let i: number = 0; i < response.items.length; ++i) {

		if (validate_babe_fee_box(tokenID, response.items[i])) {

			const responseItem: IExplorerUTXO = response.items[i];

			//following 2 lines are needed to let typescript know that both (R4, R5) registers
			//will be always defined
			if (!responseItem.additionalRegisters.R4) continue;
			if (!responseItem.additionalRegisters.R5) continue;

			const babelBox: IBabelBox = {
				boxId: responseItem.boxId,
				price: parseInt(responseItem.additionalRegisters.R5.renderedValue),
				owner: await babel_fee_sigma_prop_to_address(responseItem.additionalRegisters.R4.renderedValue),
				nanoERGs: responseItem.value,
				tokenId: extract_token_id_from_ergo_tree(responseItem.ergoTree),
				tokens: responseItem.assets
			};
			result.push(babelBox);
		}
	}

	return result;
}

export async function retrieve_babel_box_by_id(boxId: string): Promise<IBabelBox> {

	if (boxId.length !== 64) {
		throw new Error("Invalid boxId provided to validate_babel_box_id function!")
	}

	const response = await get_request(
		EXPLORER_URL + EXPLORER_BOX_BY_ID_PREFIX + boxId
	).catch(() => { throw new Error("Babel fee box retrieval failed!"); });

	const responseUTXO: IExplorerUTXO = response;

	if (
		(!(responseUTXO.ergoTree.startsWith(babelFeesContractErgoTreePrefix))) ||
		(!(responseUTXO.ergoTree.endsWith(babelFeesContractErgoTreeSuffix))) ||
		(responseUTXO.spentTransactionId !== null) ||
		(!responseUTXO.additionalRegisters.R4) || (responseUTXO.additionalRegisters.R4.sigmaType !== "SSigmaProp") ||
		(!responseUTXO.additionalRegisters.R5) || (responseUTXO.additionalRegisters.R5.sigmaType !== "SLong")
	) throw new Error("Retrieved box is not a babel fee box!");

	const babelBox: IBabelBox = {
		boxId: responseUTXO.boxId,
		price: parseInt(responseUTXO.additionalRegisters.R5.renderedValue),
		owner: await babel_fee_sigma_prop_to_address(responseUTXO.additionalRegisters.R4.renderedValue),
		nanoERGs: responseUTXO.value,
		tokenId: extract_token_id_from_ergo_tree(responseUTXO.ergoTree),
		tokens: responseUTXO.assets
	};

	return babelBox;
}
